package beans;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Building {
	
	private StringProperty BUILDING = new SimpleStringProperty();
	private StringProperty CITY = new SimpleStringProperty();
	private StringProperty COUNTRY = new SimpleStringProperty();
	private StringProperty ARCHITECT = new SimpleStringProperty();
	private StringProperty ARCHITECTURE = new SimpleStringProperty();
	private StringProperty MATERIAL = new SimpleStringProperty();
	private StringProperty IMAGE_URL = new SimpleStringProperty();
	private StringProperty ID = new SimpleStringProperty();
	private StringProperty RANK = new SimpleStringProperty();
	private StringProperty FLOORS = new SimpleStringProperty();
	private StringProperty BUILD = new SimpleStringProperty();
	private StringProperty HEIGHT_M = new SimpleStringProperty();
	private StringProperty HEIGHT_FT = new SimpleStringProperty();
	private StringProperty COST = new SimpleStringProperty();
	private StringProperty LONGITUDE = new SimpleStringProperty();
	private StringProperty LATITUDE = new SimpleStringProperty();

	
	
	public Building(String[] line){
		setID(line[0]);
		setRANK(line[1]);
		setBUILDING(line[2]);
		setCITY(line[3]);
		setCOUNTRY(line[4]);
		setHEIGHT_M(line[5]);
		setHEIGHT_FT(line[6]);
		setFLOORS(line[7]);
		setBUILD(line[8]);
		setARCHITECT(line[9]);
		setARCHITECTURE(line[10]);
		setCOST(line[11]);
		setMATERIAL(line[12]);
		setLONGITUDE(line[13]);
		setIMAGE_URL(line[14]);
		
	}


	public final StringProperty BUILDINGProperty() {
		return this.BUILDING;
	}
	


	public final String getBUILDING() {
		return this.BUILDINGProperty().get();
	}
	


	public final void setBUILDING(final String BUILDING) {
		this.BUILDINGProperty().set(BUILDING);
	}
	


	public final StringProperty CITYProperty() {
		return this.CITY;
	}
	


	public final String getCITY() {
		return this.CITYProperty().get();
	}
	


	public final void setCITY(final String CITY) {
		this.CITYProperty().set(CITY);
	}
	


	public final StringProperty COUNTRYProperty() {
		return this.COUNTRY;
	}
	


	public final String getCOUNTRY() {
		return this.COUNTRYProperty().get();
	}
	


	public final void setCOUNTRY(final String COUNTRY) {
		this.COUNTRYProperty().set(COUNTRY);
	}
	


	public final StringProperty ARCHITECTProperty() {
		return this.ARCHITECT;
	}
	


	public final String getARCHITECT() {
		return this.ARCHITECTProperty().get();
	}
	


	public final void setARCHITECT(final String ARCHITECT) {
		this.ARCHITECTProperty().set(ARCHITECT);
	}
	


	public final StringProperty ARCHITECTUREProperty() {
		return this.ARCHITECTURE;
	}
	


	public final String getARCHITECTURE() {
		return this.ARCHITECTUREProperty().get();
	}
	


	public final void setARCHITECTURE(final String ARCHITECTURE) {
		this.ARCHITECTUREProperty().set(ARCHITECTURE);
	}
	


	public final StringProperty MATERIALProperty() {
		return this.MATERIAL;
	}
	


	public final String getMATERIAL() {
		return this.MATERIALProperty().get();
	}
	


	public final void setMATERIAL(final String MATERIAL) {
		this.MATERIALProperty().set(MATERIAL);
	}
	


	public final StringProperty IMAGE_URLProperty() {
		return this.IMAGE_URL;
	}
	


	public final String getIMAGE_URL() {
		return this.IMAGE_URLProperty().get();
	}
	


	public final void setIMAGE_URL(final String IMAGE_URL) {
		this.IMAGE_URLProperty().set(IMAGE_URL);
	}
	


	public final StringProperty IDProperty() {
		return this.ID;
	}
	


	public final String getID() {
		return this.IDProperty().get();
	}
	


	public final void setID(final String ID) {
		this.IDProperty().set(ID);
	}
	


	public final StringProperty RANKProperty() {
		return this.RANK;
	}
	


	public final String getRANK() {
		return this.RANKProperty().get();
	}
	


	public final void setRANK(final String RANK) {
		this.RANKProperty().set(RANK);
	}
	


	public final StringProperty FLOORSProperty() {
		return this.FLOORS;
	}
	


	public final String getFLOORS() {
		return this.FLOORSProperty().get();
	}
	


	public final void setFLOORS(final String FLOORS) {
		this.FLOORSProperty().set(FLOORS);
	}
	


	public final StringProperty BUILDProperty() {
		return this.BUILD;
	}
	


	public final String getBUILD() {
		return this.BUILDProperty().get();
	}
	


	public final void setBUILD(final String BUILD) {
		this.BUILDProperty().set(BUILD);
	}
	


	public final StringProperty HEIGHT_MProperty() {
		return this.HEIGHT_M;
	}
	


	public final String getHEIGHT_M() {
		return this.HEIGHT_MProperty().get();
	}
	


	public final void setHEIGHT_M(final String HEIGHT_M) {
		this.HEIGHT_MProperty().set(HEIGHT_M);
	}
	


	public final StringProperty HEIGHT_FTProperty() {
		return this.HEIGHT_FT;
	}
	


	public final String getHEIGHT_FT() {
		return this.HEIGHT_FTProperty().get();
	}
	


	public final void setHEIGHT_FT(final String HEIGHT_FT) {
		this.HEIGHT_FTProperty().set(HEIGHT_FT);
	}
	


	public final StringProperty COSTProperty() {
		return this.COST;
	}
	


	public final String getCOST() {
		return this.COSTProperty().get();
	}
	


	public final void setCOST(final String COST) {
		this.COSTProperty().set(COST);
	}
	


	public final StringProperty LONGITUDEProperty() {
		return this.LONGITUDE;
	}
	


	public final String getLONGITUDE() {
		return this.LONGITUDEProperty().get();
	}
	


	public final void setLONGITUDE(final String LONGITUDE) {
		this.LONGITUDEProperty().set(LONGITUDE);
	}
	


	public final StringProperty LATITUDEProperty() {
		return this.LATITUDE;
	}
	


	public final String getLATITUDE() {
		return this.LATITUDEProperty().get();
	}
	


	public final void setLATITUDE(final String LATITUDE) {
		this.LATITUDEProperty().set(LATITUDE);
	}
	


}
