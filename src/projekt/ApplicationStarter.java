package projekt;
	
import javafx.application.Application;
import javafx.stage.Stage;
import view.ApplicationView;
import javafx.scene.Parent;
import javafx.scene.Scene;


public class ApplicationStarter extends Application {
	@Override
	public void start(Stage primaryStage) {
			Parent root = new ApplicationView();
			Scene scene = new Scene(root,1000,800);
			scene.getStylesheets().add(getClass().getResource("stylesheet.css").toExternalForm());
			primaryStage.setTitle("Buildings");
			primaryStage.setScene(scene);
			primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
