package view;

import beans.Building;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import model.ApplicationModel;

public class ApplicationView extends VBox {

	private ApplicationModel appModel;

	/**
	 * all buttons
	 */

	private Button save;
	private Button add;
	private Button delete;
	private Button undo;
	private Button redo;
	private Button ch;
	private Button gb;

	/**
	 * all labels
	 */

	private Label editorName;
	private Label editorCity;
	private Label editorHeightM;
	private Label editorFloors;
	private Label editorArchitect;
	private Label editorCost;
	private Label editorLongitude;
	private Label editorCountry;
	private Label editorHeightFT;
	private Label editorYearOfBuild;
	private Label editorStyle;
	private Label editorMaterial;
	private Label editorLatitude;

	private Label posterNumber;
	private Label posterName;
	private Label posterOrigin;
	private Label posterHeightM;

	/**
	 * all textfields
	 */

	private TextField search;
	private TextField textName;

	private TextField txtName;
	private TextField txtCity;
	private TextField txtHeightM;
	private TextField txtFloors;
	private TextField txtArchitect;
	private TextField txtCost;
	private TextField txtLongitude;
	private TextField txtCountry;
	private TextField txtHeightFt;
	private TextField txtYearOfBuild;
	private TextField txtStyle;
	private TextField txtMaterial;
	private TextField txtLatitude;

	/**
	 * ImageView for the building picture
	 */

	private ImageView picture;

	/*
	 * Boxes
	 */

	private SplitPane splitPane;
	private TableView<Building> tableView;
	private VBox rightPanel;
	private GridPane posterView;
	private GridPane editorView;

	private ToolBar toolBar;

	public ApplicationView() {
		appModel = new ApplicationModel();
		initializeControls();
		addControls();
		layoutControls();
		addBindings();

	}

	private void addBindings() {
		// TODO Auto-generated method stub

	}

	private void addControls() {

		getChildren().add(toolBar);
		getChildren().add(splitPane);
		splitPane.getItems().add(tableView);
		splitPane.getItems().add(rightPanel);
		rightPanel.getChildren().add(posterView);
		rightPanel.getChildren().add(editorView);

		posterView.setPrefHeight((tableView.getHeight()) / 2);

		posterView.add(posterNumber, 0, 0);
		posterView.add(posterName, 0, 1);
		posterView.add(posterOrigin, 0, 2);
		posterView.add(posterHeightM, 0, 3);

		// posterView.add(picture, 1, 0);

		editorView.add(txtName, 1, 0,3,1);
		editorView.add(txtCity, 1, 1);
		editorView.add(txtHeightM, 1, 2);
		editorView.add(txtFloors, 1, 3);
		editorView.add(txtArchitect, 1, 4);
		editorView.add(txtCost, 1, 5);
		editorView.add(txtLongitude, 1, 6);
		editorView.add(txtCountry, 3, 1);
		editorView.add(txtHeightFt, 3, 2);
		editorView.add(txtYearOfBuild, 3, 3);
		editorView.add(txtStyle, 3, 4);
		editorView.add(txtMaterial, 3, 5);
		editorView.add(txtLatitude, 3, 6);

		editorView.add(editorName, 0, 0);
		editorView.add(editorCity, 0, 1);
		editorView.add(editorHeightM, 0, 2);
		editorView.add(editorFloors, 0, 3);
		editorView.add(editorArchitect, 0, 4);
		editorView.add(editorCost, 0, 5);
		editorView.add(editorLongitude, 0, 6);
		editorView.add(editorCountry, 2, 1);
		editorView.add(editorHeightFT, 2, 2);
		editorView.add(editorYearOfBuild, 2, 3);
		editorView.add(editorStyle, 2, 4);
		editorView.add(editorMaterial, 2, 5);
		editorView.add(editorLatitude, 2, 6);

	}

	private void layoutControls() {

		/**
		 * specify splitpane "split"
		 */

		splitPane.setDividerPositions(0.4);
		splitPane.setPadding(new Insets(5));
		splitPane.setOrientation(Orientation.HORIZONTAL);
		VBox.setVgrow(splitPane, Priority.ALWAYS);
		splitPane.setMaxHeight(Double.MAX_VALUE);

		editorView.setGridLinesVisible(true);
		editorView.setPadding(new Insets(30));
		editorView.setVgap(10);
		editorView.setHgap(10);

		posterView.setGridLinesVisible(true);
		posterView.setPadding(new Insets(30));
		posterView.setVgap(10);
		posterView.setHgap(10);

	}

	private void initializeTableView() {

		tableView = new TableView<Building>();
		tableView.setItems(appModel.getBuilding());

		TableColumn<Building, StringProperty> rankColumn = new TableColumn<>("Rank");
		TableColumn<Building, StringProperty> buildingColumn = new TableColumn<>("Name");
		TableColumn<Building, StringProperty> cityColumn = new TableColumn<>("City");

		tableView.getColumns().add(rankColumn);
		tableView.getColumns().add(buildingColumn);
		tableView.getColumns().add(cityColumn);

		rankColumn.setCellValueFactory(new PropertyValueFactory<>("RANK"));
		buildingColumn.setCellValueFactory(new PropertyValueFactory<>("BUILDING"));
		cityColumn.setCellValueFactory(new PropertyValueFactory<>("CITY"));

	}

	private void initializeControls() {

		initializeTableView();

		rightPanel = new VBox();

		editorView = new GridPane();
		posterView = new GridPane();
		splitPane = new SplitPane();

		search = new TextField("search");

		/**
		 * initialize Buttons
		 */

		save = new Button();
		add = new Button();
		delete = new Button();
		undo = new Button();
		redo = new Button();
		ch = new Button();
		gb = new Button();

		save.setId("save-button");
		add.setId("add-button");
		delete.setId("delete-button");
		undo.setId("undo-button");
		redo.setId("redo-button");
		ch.setId("CH-button");
		gb.setId("GB-button");

		/**
		 * initialize toolBar
		 */

		toolBar = new ToolBar(save, add, delete, undo, redo, ch, gb, search);
		toolBar.setPrefHeight(140);
		toolBar.setMaxHeight(60);
		toolBar.setMinHeight(60);

		/**
		 * initialize Labels
		 */

		// To Do: replace hardcoded titles (poster... and textFields) with
		// properties

		posterNumber = new Label("31");
		posterName = new Label("Empire Bla bla");
		posterOrigin = new Label("New York bla bla");
		posterHeightM = new Label("23423");

		editorName = new Label("Name");
		editorCity = new Label("City");
		editorHeightM = new Label("Hight in Meters");
		editorFloors = new Label("Amount of floors");
		editorArchitect = new Label("Name of architect");
		editorCost = new Label("Cost");
		editorLongitude = new Label("Longitude");
		editorCountry = new Label("County");
		editorHeightFT = new Label("Height in feet");
		editorYearOfBuild = new Label("Year of build");
		editorStyle = new Label("Architecture");
		editorMaterial = new Label("Material");
		editorLatitude = new Label("Latitude");

		txtName = new TextField();
		txtCity = new TextField();
		txtHeightM = new TextField();
		txtFloors = new TextField();
		txtArchitect = new TextField();
		txtCost = new TextField();
		txtLongitude = new TextField();
		txtCountry = new TextField();
		txtHeightFt = new TextField();
		txtYearOfBuild = new TextField();
		txtStyle = new TextField();
		txtMaterial = new TextField();
		txtLatitude = new TextField();

		/**
		 * initialize toolBar
		 */

	}

}
