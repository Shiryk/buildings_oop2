package model;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import beans.Building;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ApplicationModel {

	private final ObservableList<Building> building = FXCollections.observableArrayList();

	public ApplicationModel() {
		building.addAll(readFromFile());
	}

	private List<Building> readFromFile() {
		try (Stream<String> stream = getStreamOfLines("buildings.csv", true)) {
			return stream
					.skip(1)
					.map(s -> new Building(s.split(";")))
					.collect(Collectors.toList());

		}
	}

	private Stream<String> getStreamOfLines(String fileName, boolean locatedInSameFolder) {
		try {
			return Files.lines(getPath(fileName, locatedInSameFolder), StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	private Path getPath(String fileName, boolean locatedInSameFolder) {
		try {
			if (!locatedInSameFolder) {
				fileName = "/" + fileName;
			}
			return Paths.get(getClass().getResource(fileName).toURI());
		} catch (URISyntaxException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public void save() {
		// To Do
	}
	
	public ObservableList<Building> getBuilding(){
		return building;
	}

}
